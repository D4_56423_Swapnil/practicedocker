const express = require('express')
const db = require('./db')
const utils = require('./utils')

const app = express()

app.use(express.json())

app.get('/sys', (request, response) => {
    response.send('checking...')
})


app.get('/', (request, response) => {
  const query = `
    SELECT * FROM Emp
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})


app.post('/', (request, response) => {

   const {name, salary,age} =request.body

  const query = `
    INSERT INTO Emp (name,salary, age) values ('${name}', '${salary}', ${age})
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})


app.put('/:id', (request, response) => {

  const { salary} =request.body
  const {id} =request.params

 const query = `
   UPDATE Emp SET salary  ='${salary}' where EmpId=${id}
 `
 db.execute(query, (error, result) => {
   response.send(utils.createResult(error, result))
 })
})


app.delete('/:id', (request, response) => {

 
  const {id} =request.params

 const query = `
   DELETE FROM Emp where EmpId=${id}
 `
 db.execute(query, (error, result) => {
   response.send(utils.createResult(error, result))
 })
})



app.listen(4000, '0.0.0.0', () => {
  console.log('category-server started on port 4000')
})

